package com.sharmin.BookReadingApp.core.base

data class Failure(
    val code: Int,
    val message: String
)