package com.sharmin.BookReadingApp.core.AppDatabase

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sharmin.BookReadingApp.features.bookDetails.data.local.BookListDao
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity


@Database(
    entities = [
        BookDataEntity::class
    ],
    version = 1
)
abstract class AppDatabase : RoomDatabase(){
    abstract fun bookListDao() : BookListDao
}