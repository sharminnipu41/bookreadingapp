package com.sharmin.BookReadingApp.core.module

import android.content.Context
import androidx.viewbinding.BuildConfig
import com.sharmin.BookReadingApp.core.interceptor.JwtHelper
import com.sharmin.BookReadingApp.core.interceptor.JwtInterceptor

import com.sharmin.BookReadingApp.core.interceptor.NetworkInterceptor
import com.sharmin.BookReadingApp.core.util.Const.BASE_URL
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient) : Retrofit {
        return Retrofit.Builder().apply {
            baseUrl(BASE_URL)
            addConverterFactory(GsonConverterFactory.create())
            client(client)
        }.build()
    }

    @Provides
    @Singleton
    fun provideHttpClient(networkInterceptor: NetworkInterceptor,jwtInterceptor:JwtInterceptor) : OkHttpClient {
        return OkHttpClient.Builder().apply {

            readTimeout(60, TimeUnit.SECONDS)
            writeTimeout(60, TimeUnit.SECONDS)
            connectTimeout(60, TimeUnit.SECONDS)
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            addInterceptor(networkInterceptor)
            addInterceptor(jwtInterceptor)
            addInterceptor(interceptor)
        }.build()
    }
    @Provides
    fun provideJwtInterceptor (jwtHelper: JwtHelper): JwtInterceptor {
        return JwtInterceptor(jwtHelper)
    }
    @Provides
    fun provideInterceptor(@ApplicationContext context: Context) : NetworkInterceptor {
        return NetworkInterceptor(context)
    }
}