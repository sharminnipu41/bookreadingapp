package com.sharmin.BookReadingApp.features.bookDetails.presentation.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sharmin.BookReadingApp.core.base.BaseResult
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import com.sharmin.BookReadingApp.features.bookDetails.domain.usecase.BookListUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BookListViewModel  @Inject constructor(
    private val fetchBookListUseCase :BookListUseCase
) : ViewModel() {
    private val _state = MutableStateFlow<BookListActivityState>(BookListActivityState.Init)
    val state : StateFlow<BookListActivityState> get() = _state


    private val _bookList = MutableStateFlow(mutableListOf<BookDataEntity>())
    val bookList : StateFlow<List<BookDataEntity>> get() = _bookList

    private fun setLoading(){
        _state.value = BookListActivityState.IsLoading(true)
    }

    private fun hideLoading(){
        _state.value = BookListActivityState.IsLoading(false)
    }

    private fun showToast(message: String){
        _state.value =BookListActivityState.ShowToast(message)
    }

    fun fetchBookList(){
        viewModelScope.launch {
            fetchBookListUseCase.invoke()
                .onStart {
                    setLoading()
                }
                .catch { e ->
                    hideLoading()
                    showToast(e.message.toString())
                }
                .collect { result ->
                    hideLoading()
                    when(result) {
                        is BaseResult.Success -> {
                            _bookList.value = result.data as MutableList<BookDataEntity>
                        }
                        is BaseResult.Error -> {
                            // 0 means no internet connection
                            if(result.err.code != 0){
                                val msg = "${result.err.message} [${result.err.code}]"
                                showToast(msg)
                            }else{
                                showToast("No Internet!!")
                            }

                        }
                    }
                }
        }
    }
}

sealed class BookListActivityState {
    object Init : BookListActivityState()
    data class ShowToast(val message : String) : BookListActivityState()
    data class IsLoading(val isLoading: Boolean) : BookListActivityState()
}