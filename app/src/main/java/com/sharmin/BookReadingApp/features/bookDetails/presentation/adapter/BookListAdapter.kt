package com.sharmin.BookReadingApp.features.bookDetails.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.sharmin.BookReadingApp.databinding.ItemBookListBinding
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import kotlinx.coroutines.withContext

class BookListAdapter (private val bookList: MutableList<BookDataEntity>) : RecyclerView.Adapter<BookListAdapter.ViewHolder>() {
    interface Listener {
        fun onTap(book: BookDataEntity)
    }

    private var listener: Listener? = null


    fun setOnTapListener(l: Listener) {
        listener = l
    }


    fun updateList(list: List<BookDataEntity>) {
        bookList.clear()
        bookList.addAll(list)
        notifyDataSetChanged()
    }


    inner class ViewHolder(private val itemBinding: ItemBookListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(book: BookDataEntity) {
            itemBinding.tvBookName.text=book.name

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = ItemBookListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(bookList[position])

    override fun getItemCount() = bookList.size
}