package com.sharmin.BookReadingApp.features.bookDetails.data.remote.api

import com.sharmin.BookReadingApp.features.bookDetails.data.remote.dto.BookListResponse
import retrofit2.Response
import retrofit2.http.GET

interface BookListApi {
    @GET("api/book-list")
    suspend fun bookListFetch() : Response<BookListResponse>
}