package com.sharmin.BookReadingApp.features.bookDetails.domain.repository

import com.sharmin.BookReadingApp.core.base.BaseResult
import com.sharmin.BookReadingApp.core.base.Failure
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import kotlinx.coroutines.flow.Flow

interface BookListRepository {

    suspend fun fetchBookList() : Flow<BaseResult<List<BookDataEntity>, Failure>>
}
