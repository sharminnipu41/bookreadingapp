package com.sharmin.BookReadingApp.features.bookDetails.data.di

import com.sharmin.BookReadingApp.core.AppDatabase.AppDatabase
import com.sharmin.BookReadingApp.features.bookDetails.data.local.BookListDao
import com.sharmin.BookReadingApp.features.bookDetails.data.remote.BookListFetchingRemoteSource
import com.sharmin.BookReadingApp.features.bookDetails.data.remote.api.BookListApi
import com.sharmin.BookReadingApp.features.bookDetails.data.repositoryImpl.BookListFetchingRepositoryImpl
import com.sharmin.BookReadingApp.features.bookDetails.domain.repository.BookListRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object BookListFetchModule {

    @Provides
    @Singleton
    fun provideTodoRemoteApi(retrofit: Retrofit) : BookListApi {
        return retrofit.create(BookListApi::class.java)
    }

    @Provides
    @Singleton
    fun provideBookListRemoteSource(bookListApi: BookListApi) : BookListFetchingRemoteSource {
        return BookListFetchingRemoteSource(bookListApi)
    }

    @Provides
    @Singleton
    fun provideBookListDao(appDatabase:AppDatabase) : BookListDao {
        return appDatabase.bookListDao()
    }

    @Provides
    @Singleton
    fun provideBookListRepository(bookListRemoteSource: BookListFetchingRemoteSource, bookListDao: BookListDao) : BookListRepository {
        return BookListFetchingRepositoryImpl(bookListRemoteSource,bookListDao)
    }
}