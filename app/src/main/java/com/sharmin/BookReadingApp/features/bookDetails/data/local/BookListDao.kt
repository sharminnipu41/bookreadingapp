package com.sharmin.BookReadingApp.features.bookDetails.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity


@Dao
interface BookListDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(todo: BookDataEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(todos: List<BookDataEntity>)

    @Query("SELECT * FROM BookDetails")
    fun findAll() : List<BookDataEntity>

    @Query("DELETE FROM BookDetails")
    fun deleteAll()

}
