package com.sharmin.BookReadingApp.features.bookDetails.data.remote.dto

import com.google.gson.annotations.SerializedName

data class BookListResponse (
    @SerializedName("status"  ) var status  : Int?            = null,
    @SerializedName("message" ) var message : String?         = null,
    @SerializedName("data"    ) var data    : ArrayList<BookData> = arrayListOf()
)