package com.sharmin.BookReadingApp.features.bookDetails.presentation.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.sharmin.BookReadingApp.databinding.ActivityBookListBinding
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import com.sharmin.BookReadingApp.features.bookDetails.presentation.adapter.BookListAdapter
import com.sharmin.BookReadingApp.features.bookDetails.presentation.viewModel.BookListActivityState
import com.sharmin.BookReadingApp.features.bookDetails.presentation.viewModel.BookListViewModel


import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class BookListsActivity : AppCompatActivity() {
    private lateinit var binding : ActivityBookListBinding

    private val viewModel: BookListViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityBookListBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupRecyclerView()
        observe()
        fetchTodos()
    }

    private fun setupRecyclerView() {
        val a = BookListAdapter(mutableListOf())
        a.setOnTapListener(object: BookListAdapter.Listener{
            override fun onTap(bookList: BookDataEntity) {
                startActivity(Intent(this@BookListsActivity,BookDetailsActivity::class.java).apply {
                    putExtra("bookDetails",bookList)
                })
                //println(bookList.name)
            }
        })

        binding.bookListRecyclerView.apply {
            adapter = a
            layoutManager = LinearLayoutManager(this@BookListsActivity)
        }
    }

    private fun observe(){
        observeState()
        observeTodos()
    }

    private fun observeState(){
        viewModel.state.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { handleState(it) }
            .launchIn(lifecycleScope)
    }

    private fun observeTodos(){
        viewModel.bookList.flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { handleTodos(it) }
            .launchIn(lifecycleScope)
    }

    private fun fetchTodos(){
        viewModel.fetchBookList()
    }

    private fun handleState(state : BookListActivityState){
        when(state){
            is BookListActivityState.ShowToast -> Toast.makeText(this@BookListsActivity, state.message, Toast.LENGTH_LONG).show()
            is BookListActivityState.IsLoading -> handleLoading(state.isLoading)
            is BookListActivityState.Init -> Unit
        }
    }

    private fun handleLoading(isLoading : Boolean){
        if(isLoading){
            binding.loadingBar.visibility = View.VISIBLE
        }else{
            binding.loadingBar.visibility = View.GONE
        }
    }

    private fun handleTodos(books: List<BookDataEntity>){
        binding.bookListRecyclerView.adapter?.let { adapter ->
            if(adapter is BookListAdapter){
                adapter.updateList(books)
            }
        }
    }
}