package com.sharmin.BookReadingApp.features.bookDetails.data.remote.dto

import com.google.gson.annotations.SerializedName

data class BookData(
    @SerializedName("id"    ) var id    : Int?    = null,
    @SerializedName("name"  ) var name  : String? = null,
    @SerializedName("image" ) var image : String? = null,
    @SerializedName("url"   ) var url   : String? = null

)
