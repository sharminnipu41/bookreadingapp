package com.sharmin.BookReadingApp.features.bookDetails.domain.entity

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Entity(tableName = "BookDetails", indices = [Index(value = ["id"], unique = true)])
@Parcelize
data class BookDataEntity(
    @PrimaryKey(autoGenerate = false)
    var id : Int,
    @ColumnInfo(name = "name")
    var name: String,
    @ColumnInfo(name = "image")
    var image: String,
    @ColumnInfo(name = "url")
    var url: String
):Parcelable