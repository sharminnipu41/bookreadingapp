package com.sharmin.BookReadingApp.features.bookDetails.data.repositoryImpl

import com.sharmin.BookReadingApp.core.base.BaseResult
import com.sharmin.BookReadingApp.core.base.Failure
import com.sharmin.BookReadingApp.features.bookDetails.data.local.BookListDao
import com.sharmin.BookReadingApp.features.bookDetails.data.remote.BookListFetchingRemoteSource
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import com.sharmin.BookReadingApp.features.bookDetails.domain.repository.BookListRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class BookListFetchingRepositoryImpl constructor(
    private val bookListRemoteSource: BookListFetchingRemoteSource,
    private val bookListDao: BookListDao
) : BookListRepository {

    override suspend fun fetchBookList(): Flow<BaseResult<List<BookDataEntity>, Failure>> {
        return flow {
            val localBookList = bookListDao.findAll()

            // send the local db value to db
            emit(BaseResult.Success(localBookList))

            // is should fetch?
            if(localBookList.isNullOrEmpty()){
                val result = bookListRemoteSource.fetchBookList()
                if(result is BaseResult.Success){
                    saveToLocal(result.data)
                }
                emit(result)
            }
        }

    }

    private fun saveToLocal(todos: List<BookDataEntity>){
        bookListDao.deleteAll()
        bookListDao.insertAll(todos)
    }
}