package com.sharmin.BookReadingApp.features.bookDetails.presentation.view

import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import com.bumptech.glide.Glide
import com.sharmin.BookReadingApp.R
import com.sharmin.BookReadingApp.databinding.ActivityBookDetailsBinding
import com.sharmin.BookReadingApp.databinding.ActivityBookListBinding
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity

class BookDetailsActivity : AppCompatActivity() {
     private lateinit   var mydata:BookDataEntity
    private lateinit var binding : ActivityBookDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =ActivityBookDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mydata = intent?.getParcelableExtra("bookDetails")!!

        initialView()


    }

    private fun initialView(){
        Glide.with(this)
            .load(mydata.image)
            .into(binding.ivBook)

        binding.bookNameTv.text=mydata.name

        binding.onlineBookReading.settings.allowFileAccess = true
        binding.onlineBookReading.settings.domStorageEnabled = true
        binding.onlineBookReading.settings.builtInZoomControls = true
        binding.onlineBookReading.clearCache(true)
        binding.onlineBookReading.settings.defaultTextEncodingName = "utf-8"
        binding.onlineBookReading.requestFocus()
        binding.onlineBookReading.settings.javaScriptEnabled = true
        binding.onlineBookReading.loadUrl(mydata.url)
        binding.onlineBookReading.webChromeClient = WebChromeClient()
        binding.onlineBookReading.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)

                Log.e("onPageStarted","onPageStarted")
            }
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                Log.e("onPageFinish","onPageFinish")

            }
            @Deprecated("Deprecated in Java")
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url!!)
                return true

            }
        }

    }
}