package com.sharmin.BookReadingApp.features.bookDetails.domain.usecase

import com.sharmin.BookReadingApp.core.base.BaseResult
import com.sharmin.BookReadingApp.core.base.Failure
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity
import com.sharmin.BookReadingApp.features.bookDetails.domain.repository.BookListRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookListUseCase @Inject constructor(private val bookListRepository: BookListRepository) {
    suspend fun invoke() : Flow<BaseResult<List<BookDataEntity>, Failure>> {
        return bookListRepository.fetchBookList()
    }
}