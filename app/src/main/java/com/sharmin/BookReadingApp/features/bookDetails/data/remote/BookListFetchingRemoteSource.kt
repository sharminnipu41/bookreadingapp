package com.sharmin.BookReadingApp.features.bookDetails.data.remote

import com.sharmin.BookReadingApp.core.base.BaseResult
import com.sharmin.BookReadingApp.core.base.Failure
import com.sharmin.BookReadingApp.core.exception.NoInternetConnectionException
import com.sharmin.BookReadingApp.features.bookDetails.data.remote.api.BookListApi
import com.sharmin.BookReadingApp.features.bookDetails.domain.entity.BookDataEntity

class BookListFetchingRemoteSource constructor(private val bookListApi:BookListApi) {
    suspend fun fetchBookList() : BaseResult<List<BookDataEntity>, Failure> {
        try{
            val response = bookListApi.bookListFetch()
            return if(response.isSuccessful){
                val books = mutableListOf<BookDataEntity>()
                response.body()?.data?.forEach { book ->
                   books.add(BookDataEntity(book.id!!, book.name!!, book.image!!, book.url!!))

                    // todos.add(TodoEntity(todo.id, todo.title, todo.isCompleted))
                }
                BaseResult.Success(books)
            }else{
                //i will create a basic error here
                BaseResult.Error(Failure(response.code(), response.message()))
            }
        }catch (e: NoInternetConnectionException){
            return BaseResult.Error(Failure(0, e.message))
        }catch (e: Exception){
            return BaseResult.Error(Failure(-1, e.message.toString()))
        }
    }
}